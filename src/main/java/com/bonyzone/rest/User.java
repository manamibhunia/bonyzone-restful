package com.bonyzone.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.bonyzone.rest.jdbc.UserDAO;
import com.bonyzone.rest.model.UserTO;
import com.bonyzone.rest.util.HashUtil;
import com.sun.jersey.api.view.Viewable;

@Path("/User")
public class User {


    private static Logger logger = Logger.getLogger(User.class);
    
	@GET
	@Path("details/{email}")
	public Response getMsg(@PathParam("email") String email) {

		String output = "Jersey say : " + email;

		return Response.status(200).entity(output).build();
	}

	@POST
	@Path("login")
	@Consumes()
	@Produces(MediaType.APPLICATION_JSON)
	public Viewable login(@Context HttpServletRequest request, @Context HttpServletResponse response, 
			@FormParam("email") String email, @FormParam("password") String password, @FormParam("item") String item) {
 
		UserDAO userDAO = new UserDAO();
		try {
			UserTO dbUser = userDAO.findUserByEmail(email);
			System.out.println("dbUser- "+dbUser);
			if(HashUtil.check(password, dbUser.getPassword())) {
				UserTO user = new UserTO();
				user.setEmail(email);
				user.setUserId(dbUser.getUserId());
				user.setFirstName(dbUser.getFirstName());
				user.setLastName(dbUser.getLastName());
				request.setAttribute("user", user);
				request.setAttribute("item", item);
				return new Viewable("/transaction.jsp", null);
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
			request.setAttribute("message", "Wrong credentials");
			return new Viewable("/login.jsp", null);
		}
		request.setAttribute("message", "Wrong credentials");
		return new Viewable("/login.jsp", null);
	}

	@POST
	@Path("register")
	@Consumes()
	public Viewable register(@Context HttpServletRequest request, @Context HttpServletResponse response, 
			@FormParam("first-name") String firstName, @FormParam("last-name") String lastName, 
			@FormParam("address") String address, @FormParam("email") String email,
			@FormParam("password") String password, @FormParam("item") String item) {

		try {
			UserTO user = new UserTO( firstName, lastName, HashUtil.getSaltedHash(password), email, address);
			UserDAO userDAO = new UserDAO();
			userDAO.createUser(user);
			request.setAttribute("user", user);
			request.setAttribute("item", item);
			return new Viewable("/transaction.jsp", null);
		} catch (Exception e) {
			logger.debug(e.getMessage());
			request.setAttribute("message", "Wrong credentials");
			return new Viewable("/login.jsp", null);
		}
	}

	@POST
	@Path("updateCc")
	@Consumes()
	public Viewable updateCc(@Context HttpServletRequest request, @Context HttpServletResponse response, 
			@FormParam("email") String email, @FormParam("cc") String cc) {

		return new Viewable("/transaction.jsp", null);
	}

//	private UserTO toObject(String jsonStr) throws JsonParseException,
//			JsonMappingException, IOException {
//
//		ObjectMapper mapper = new ObjectMapper();
//		return mapper.readValue(jsonStr, UserTO.class);
//	}
//
//	private String toJSON(UserTO user) throws JsonParseException,
//			JsonMappingException, IOException {
//
//		ObjectMapper mapper = new ObjectMapper();
//		return mapper.writeValueAsString(user);
//	}
}