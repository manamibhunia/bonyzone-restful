<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="<%=request.getContextPath()%>/images/favicon.ico">
		<title>Bonyzone</title>
		<link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<%=request.getContextPath()%>/css/login.css" rel="stylesheet">
		<link href="<%=request.getContextPath()%>/css/menu.css" rel="stylesheet">
	</head>

	<body>
	
		<nav class="navbar navbar-inverse navbar-fixed-top top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand custom" href="#"><i class="glyphicon glyphicon-gift"></i>Bonyzone</a>
				</div>
		
				<div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<section id="user-login">
		    <div class="container">
		    	<div class="row login-form">
		    	    <div class="col-xs-12">
		        	    <div class="form-wrap">
		                	<h1>Log in with your email id</h1>
		                	<form class="form-horizontal" role="form" action="<%=request.getContextPath()%>/rest/User/login" method="POST">
		                        <div class="form-group">
		                            <label for="email" class="sr-only">Email</label>
		                            <input type="email" name="email" id="email" class="form-control" placeholder="-----@----.com" required>
		                            <p class="help-block"></p>
		                        </div>
		                        <div class="form-group">
		                            <label for="password" class="sr-only">Password</label>
		                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
		                            <p class="help-block"></p>
		                        </div>
		                        <div class="checkbox">
		                            <span class="show-checkbox show-password"></span>
		                            <span class="label">Show password</span>
		                        </div>
		                        <input type="hidden" name="item" value="<%= request.getParameter("item")%>"/>
		                        <button id="login" type="submit" class="btn btn-custom btn-lg btn-block login-submit" value="Log in">Login</button>
		                	</form>
		                	<button id="register" class="btn btn-lg btn-block" value="Log in">Register</button>
		                	<div class="error-message"><%= (String)request.getAttribute("message") == null ? "" : (String)request.getAttribute("message")%></div>
		        	    </div>
		    		</div> <!-- /.col-xs-12 -->
		    	</div> <!-- /login-form .row -->
		    	
		    	
		    	<div class="row register-form">
		    	    <form class="form-horizontal" role="form" action="<%=request.getContextPath()%>/rest/User/register" method="POST">
		    	    
					  <div class="form-group">
					    <label for="first-name" class="col-sm-2 control-label">First Name</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" name="first-name" id="first-name" placeholder="First Name" required>
					      <p class="help-block"></p>
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="last-name" class="col-sm-2 control-label">Last Name</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" name="last-name" id="last-name" placeholder="Last Name">
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="email-id" class="col-sm-2 control-label">Email</label>
					    <div class="col-sm-10">
					      <input type="email" class="form-control" name="email" id="email-id" placeholder="Email" required>
					      <p class="help-block"></p>
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="email-id2" class="col-sm-2 control-label">Enter Email again</label>
					    <div class="col-sm-10">
					      <input type="email" class="form-control" id="email-id2" placeholder="Email" 
					      		data-validation-matches-match="email"
								data-validation-matches-message="Must match email address entered above" required>
					      <p class="help-block"></p>
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="password" class="col-sm-2 control-label">Password</label>
					    <div class="col-sm-10">
					      <input type="password" class="form-control" name="password" id="rg-password" placeholder="Password" required minlength="10">
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="password2" class="col-sm-2 control-label">Re-type Password</label>
					    <div class="col-sm-10">
					      <input type="password" class="form-control" id="rg-password2" placeholder="Password" 
					      		data-validation-matches-match="password"
								data-validation-matches-message="Must match password entered above" required>
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label for="address" class="col-sm-2 control-label">Address</label>
					    <div class="col-sm-10">
					      <textarea class="form-control" name="address" id="address" placeholder="Address" required></textarea>
					    </div>
					  </div>
					  
					  <div class="form-group pull-right">
					    <div class="col-sm-offset-2 col-sm-10">
		                  <input type="hidden" name="item" value="<%= request.getParameter("item")%>"/>
					      <button type="submit" class="btn btn-default submit-registration">Sign Up</button>
					    </div>
					  </div>
					</form> <!-- /.form-horizontal -->
		    	</div> <!-- /.row -->
		    </div> <!-- /.container -->
		</section>

		<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
		<script src="<%=request.getContextPath()%>/js/jqBootstrapValidation.js"></script>
		<script src="<%=request.getContextPath()%>/js/login.js"></script>
	</body>
</html>
