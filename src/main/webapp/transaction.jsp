<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="<%=request.getContextPath()%>/images/favicon.ico">
		<title>Bonyzone</title>
		<link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<%=request.getContextPath()%>/css/font-awesome.min.css" rel="stylesheet">
		<link href="<%=request.getContextPath()%>/css/transaction.css" rel="stylesheet">
		<link href="<%=request.getContextPath()%>/css/menu.css" rel="stylesheet">
	</head>

	<body>
<%= (String)request.getAttribute("item")%>

		<nav class="navbar navbar-inverse navbar-fixed-top top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand custom" href="#"><i class="glyphicon glyphicon-gift"></i>Bonyzone</a>
				</div>
		
				<div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="#">Logout</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>


		<div class="container-fluid transaction-form">
			<div class="col-md-6">
			</div>
			<div class="col-md-6">
				<h3>Shipping Address</h3>
				<form class="form-horizontal" role="form">
					<div class="row">
						<div class="form-group">
					    	<div class="input-group">
					      		<label class="sr-only" for="fullName">Name</label>
					      		<div class="input-group-addon"><i class=" fa fa-user"></i></div>
					      		<input type="email" class="form-control" name="fullName" id="fullName" placeholder="Full name">
					    	</div>
					  	</div>
						<div class="form-group">
					    	<div class="input-group">
					      		<label class="sr-only" for="email">Email address</label>
					      		<div class="input-group-addon">@</div>
					      		<input type="email" class="form-control" name="email" id="email" placeholder="Email">
					    	</div>
					  	</div>
						<div class="form-group">
					    	<div class="input-group">
					      		<label class="sr-only" for="exampleInputEmail2">Phone Number</label>
					      		<div class="input-group-addon"><i class=" fa fa-phone"></i></div>
					      		<input type="email" class="form-control" id="phone" placeholder="Phone Number">
					    	</div>
					  	</div>
						<div class="form-group">
					    	<div class="input-group">
					      		<label class="sr-only" for="exampleInputEmail2">Address</label>
					      		<div class="input-group-addon"><i class=" fa fa-envelope"></i></div>
					      		<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Full Address">
					    	</div>
					  	</div>
						<div class="form-group">
					    	<div class="input-group">
					      		<label class="sr-only" for="exampleInputEmail2">Name on Card</label>
					      		<div class="input-group-addon"><i class=" fa fa-user"></i></div>
					      		<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Name on card">
					    	</div>
					  	</div>
						<div class="form-group">
					    	<div class="input-group">
					      		<label class="sr-only" for="exampleInputEmail2">Card Number</label>
					      		<div class="input-group-addon"><i class=" fa fa-credit-card"></i></div>
					      		<input name="card" class="form-control" placeholder="Card number" type="text">
					    	</div>
					  	</div>
					</div>
					
					<div class="row">
					  	
						<div class="inline">
							<input name="radio-inline" checked="" type="radio"><i class="fa fa-cc-visa"></i>
							<input name="radio-inline" type="radio"><i class="fa fa-cc-mastercard"></i>
							<input name="radio-inline" type="radio"><i class="fa fa-cc-amex"></i>
					  	</div>
						<div class="inline">
					      	<input name="cvv" class="form-control" placeholder="CVV2" data-mask="999" type="text">
					    </div>
						<div class="inline">
							<span class="col-md-6">
					      		<select name="month" class="form-control">
									<option value="0" selected="" disabled="">Month</option>
									<option value="1">January</option>
									<option value="1">February</option>
									<option value="3">March</option>
									<option value="4">April</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">August</option>
									<option value="9">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</span>
							<span class="col-md-6">
					      		<input name="year" class="form-control" placeholder="Year" data-mask="2099" type="text">
					      	</span>
					  	</div>
					</div>
				</form>
			</div>
		</div>

		<script src="<%=request.getContextPath()%>/js/jquery-2.1.1.min.js"></script>
		<script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
	</body>
</html>
