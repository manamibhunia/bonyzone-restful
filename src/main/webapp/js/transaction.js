$(function(){

	$('.show-password').click(function(event){
	    var key_attr = $('#password').attr('type');
	    if(key_attr != 'text') {
	        
	        $('.checkbox').addClass('show');
	        $('#password').attr('type', 'text');
	    } else {
	        $('.checkbox').removeClass('show');
	        $('#password').attr('type', 'password');
	    }
	});
	
	$("input,select,textarea").not(".login-submit").jqBootstrapValidation();
	
	$("input,select,textarea").not(".submit-registration").jqBootstrapValidation();
	$('#register').click(function(event){
		event.preventDefault();
		$('.login-form').hide();
		$('.register-form').show();
	});
	
});